export type Ingredient = {
  id: number;
  name: string;
};

export type Recipe = {
  id: number;
  name: string;
  description: string;
  image: string;
  ingredients: IngredientEntry[];
};

export type IngredientEntry = {
  idIngredient: number;
  quantity: number;
};

export type User = {
  username: string;
  email: string;
  token: string;
};
