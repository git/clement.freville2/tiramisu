import { Injectable } from '@angular/core';
import { Recipe } from '../cookbook/type';

@Injectable({
  providedIn: 'root',
})
export class DeliveryService {
  readonly #shoppingCart: Recipe[];

  constructor() {
    this.#shoppingCart = JSON.parse(localStorage.getItem('shoppingCart') || '[]');
  }

  addToCart(recipe: Recipe): void {
    this.#shoppingCart.push(recipe);
    localStorage.setItem('shoppingCart', JSON.stringify(this.#shoppingCart));
  }

  get shoppingCart(): readonly Recipe[] {
    return this.#shoppingCart;
  }
}
