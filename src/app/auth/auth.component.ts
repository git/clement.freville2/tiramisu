import { Component } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatCard, MatCardContent, MatCardTitle } from '@angular/material/card';
import { MatFormField } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [ReactiveFormsModule, MatCard, MatCardTitle, MatCardContent, MatFormField, MatInput],
  templateUrl: './auth.component.html',
})
export class AuthComponent {
  loginForm = this.formBuilder.group({
    login: '',
    password: '',
  });

  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private router: Router,
    activatedRoute: ActivatedRoute,
  ) {
    activatedRoute.data.subscribe(({ registering }) => {
      if (!registering) {
        loginService.logOut();
      }
    });
  }

  onSubmit(): void {
    if (this.loginForm.invalid) {
      return;
    }
    const value = this.loginForm.value;
    this.loginService.logIn(value.login!, value.password!)
      .then((logged) => {
        if (logged) {
          this.router.navigateByUrl('/ingredients');
        } else {
          alert('Invalid login!');
        }
      });
  }
}
