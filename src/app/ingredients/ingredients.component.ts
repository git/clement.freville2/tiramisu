import { Component } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatPaginator } from '@angular/material/paginator';
import {
  MatCell,
  MatCellDef,
  MatColumnDef,
  MatHeaderCell,
  MatHeaderCellDef,
  MatHeaderRow,
  MatHeaderRowDef,
  MatRow,
  MatRowDef,
  MatTable,
  MatTableDataSource,
} from '@angular/material/table';
import { RouterLink } from '@angular/router';
import { Ingredient } from '../../cookbook/type';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-ingredients',
  standalone: true,
  imports: [
    MatButton,
    MatCell,
    MatCellDef,
    MatColumnDef,
    MatHeaderCell,
    MatHeaderRow,
    MatHeaderRowDef,
    MatPaginator,
    MatRow,
    MatRowDef,
    MatTable,
    MatHeaderCellDef,
    RouterLink,
  ],
  templateUrl: './ingredients.component.html',
})
export class IngredientsComponent {
  displayedColumns: string[] = ['id', 'name', 'actions'];
  dataSource = new MatTableDataSource<Ingredient>();

  constructor(protected recipes: RecipeService) {
    this.dataSource = new MatTableDataSource<Ingredient>(recipes.getAllIngredients());
  }

  delete(ingredient: Ingredient): void {
    this.recipes.deleteIngredient(ingredient);
    this.dataSource.data = this.recipes.getAllIngredients();
  }
}
