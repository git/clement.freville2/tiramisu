import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { Router, RouterLink } from '@angular/router';
import { Recipe } from '../../cookbook/type';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe',
  standalone: true,
  imports: [CommonModule, MatIconModule, MatButtonModule, RouterLink],
  templateUrl: './recipe.component.html',
})
export class RecipeComponent {
  recipe: Recipe = { id: -1, name: '', description: '', image: '', ingredients: [] };

  @Input()
  set id(id: string) {
    this.recipe = this.recipes.get(parseInt(id))!;
  }

  constructor(protected recipes: RecipeService, private router: Router) {
  }

  delete(): void {
    this.recipes.delete(this.recipe);
    this.router.navigateByUrl('/recipes');
  }
}
