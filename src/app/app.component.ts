import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { DeliveryService } from './delivery.service';
import { LoginService } from './login.service';
import { RecipeService } from './recipe.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, RouterLinkActive, MatMenuModule, MatButtonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  providers: [RecipeService, LoginService, DeliveryService],
})
export class AppComponent {
  title = 'tiramisu';

  constructor(protected recipes: RecipeService, protected login: LoginService) {
  }
}
