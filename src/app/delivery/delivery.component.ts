import { Component } from '@angular/core';
import { DeliveryService } from '../delivery.service';

@Component({
  selector: 'app-delivery',
  standalone: true,
  imports: [],
  templateUrl: './delivery.component.html',
})
export class DeliveryComponent {
  constructor(protected delivery: DeliveryService) {}
}
