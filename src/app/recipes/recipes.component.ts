import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { RouterLink } from '@angular/router';
import { Recipe } from '../../cookbook/type';
import { DeliveryService } from '../delivery.service';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipes',
  standalone: true,
  imports: [MatTableModule, MatPaginatorModule, RouterLink, MatButton],
  templateUrl: './recipes.component.html',
})
export class RecipesComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'description', 'image', 'actions'];
  dataSource = new MatTableDataSource<Recipe>();

  constructor(protected recipes: RecipeService, private delivery: DeliveryService) {
    this.dataSource = new MatTableDataSource<Recipe>(recipes.getAll());
  }

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  deliver(recipe: Recipe): void {
    this.delivery.addToCart(recipe);
  }
}
