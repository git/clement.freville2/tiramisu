import { inject } from '@angular/core';
import { CanActivateFn, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { IngredientAddComponent } from './ingredient-add/ingredient-add.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { LoginService } from './login.service';
import { RecipeAddComponent } from './recipe-add/recipe-add.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipesComponent } from './recipes/recipes.component';

const LoggedGuard: CanActivateFn = () => inject(LoginService).isLoggedIn();
export const routes: Routes = [
  { path: '', component: DeliveryComponent, pathMatch: 'full' },
  { path: 'recipes', component: RecipesComponent },
  { path: 'recipe/add', component: RecipeAddComponent },
  { path: 'recipe/:id', component: RecipeComponent },
  { path: 'recipe/:id/edit', component: RecipeAddComponent },
  { path: 'ingredients', component: IngredientsComponent, canActivate: [LoggedGuard] },
  { path: 'ingredients/add', component: IngredientAddComponent, canActivate: [LoggedGuard] },
  { path: 'ingredients/:id/edit', component: IngredientAddComponent, canActivate: [LoggedGuard] },
  { path: 'login', component: AuthComponent },
  { path: 'logout', component: AuthComponent, data: { registering: false }, canActivate: [LoggedGuard] },
];
