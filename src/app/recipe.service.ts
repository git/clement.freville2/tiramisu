import { Injectable } from '@angular/core';
import { Ingredient, Recipe } from '../cookbook/type';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  #recipes: Recipe[] = [
    {
      id: 0,
      name: 'crepe1',
      description: 'La meilleure recette de pâte à crêpes',
      image: '',
      ingredients: [
        { idIngredient: 1, quantity: 10 },
        { idIngredient: 2, quantity: 15 },
      ],
    },
    { id: 1, name: 'crepe2', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 2, name: 'crepe3', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 3, name: 'crepe4', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 4, name: 'crepe5', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 5, name: 'crepe6', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 6, name: 'crepe7', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 7, name: 'crepe8', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 8, name: 'crepe9', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 9, name: 'crepe10', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 10, name: 'crepe11', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 11, name: 'crepe12', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 12, name: 'crepe13', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
    { id: 13, name: 'crepe14', description: 'La meilleure recette de pâte à crêpes', image: '', ingredients: [] },
  ];

  #ingredients: Ingredient[] = [
    { id: 1, name: 'Sucre' },
    { id: 2, name: 'Farine' },
  ];

  constructor() {
    if (environment.API_URL) {
      fetch(environment.API_URL)
        .then((response) => response.json())
        .then((data) => {
          for (const ingredient of data) {
            ingredient.id = parseInt(ingredient.id);
          }
          this.#ingredients.push(...data);
        });
    }
    const localRecipes = localStorage.getItem('recipes');
    if (localRecipes) {
      this.#recipes = JSON.parse(localRecipes);
    }
  }

  getAll(): Recipe[] {
    return this.#recipes;
  }

  getAllIngredients(): Ingredient[] {
    return this.#ingredients;
  }

  get(id: number): Recipe | null {
    return this.#recipes.find((recipe) => recipe.id === id) || null;
  }

  getIngredientById(id: number): Ingredient | null {
    return this.#ingredients.find((ingredient) => ingredient.id === id) || null;
  }

  add(recipe: Omit<Recipe, 'id'>): void {
    const id = this.#recipes.length ? Math.max(...this.#recipes.map((recipe) => recipe.id)) + 1 : 1;
    this.#recipes.push({
      id,
      ...recipe,
    });
    this.syncRecipes();
  }

  edit(recipe: Recipe): void {
    for (let i = 0; i < this.#recipes.length; ++i) {
      if (this.#recipes[i].id === recipe.id) {
        this.#recipes[i] = recipe;
      }
    }
    this.syncRecipes();
  }

  delete(recipe: Recipe): boolean {
    const index = this.#recipes.findIndex((v) => v.id === recipe.id);
    if (index === -1) {
      return false;
    }
    this.#recipes.splice(index, 1);
    this.syncRecipes();
    return true;
  }

  syncRecipes() {
    localStorage.setItem('recipes', JSON.stringify(this.#recipes));
  }

  addIngredient(ingredient: Omit<Ingredient, 'id'>) {
    const id = this.#ingredients.length ? Math.max(...this.#ingredients.map((ingredient) => ingredient.id)) + 1 : 1;
    this.#ingredients.push({
      id,
      ...ingredient,
    });
  }

  editIngredient(ingredient: Ingredient) {
    for (let i = 0; i < this.#ingredients.length; ++i) {
      if (this.#ingredients[i].id === ingredient.id) {
        this.#ingredients[i] = ingredient;
      }
    }
  }

  deleteIngredient(ingredient: Ingredient) {
    const index = this.#ingredients.findIndex((v) => v.id === ingredient.id);
    if (index === -1) {
      return;
    }
    this.#ingredients.splice(index, 1);
  }
}
