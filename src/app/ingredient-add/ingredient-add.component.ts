import { Component, Input } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { MatOption } from '@angular/material/core';
import { MatError, MatFormField } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { MatSelect } from '@angular/material/select';
import { Router } from '@angular/router';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-ingredient-add',
  standalone: true,
  imports: [
    MatButton,
    MatError,
    MatFormField,
    MatInput,
    MatOption,
    MatSelect,
    ReactiveFormsModule,
  ],
  templateUrl: './ingredient-add.component.html',
})
export class IngredientAddComponent {
  createForm = this.formBuilder.group({
    name: '',
  });

  #ingredientId: number = -1;
  @Input()
  set id(recipeId: string) {
    if (recipeId === undefined) return;
    this.#ingredientId = parseInt(recipeId);
    const ingredient = this.recipes.getIngredientById(this.#ingredientId);
    if (ingredient === null) {
      this.router.navigateByUrl('404');
      return;
    }
    this.createForm.patchValue({
      name: ingredient.name,
    });
  }
  get ingredientId() {
    return this.#ingredientId;
  }

  constructor(private formBuilder: FormBuilder, private recipes: RecipeService, private router: Router) {
  }

  onSubmit() {
    if (this.createForm.invalid) {
      return;
    }
    const value = this.createForm.value;
    if (this.ingredientId !== -1) {
      this.recipes.editIngredient({ id: this.ingredientId, name: value.name! });
    } else {
      this.recipes.addIngredient({ name: value.name! });
    }
    this.router.navigateByUrl('/ingredients');
  }
}
