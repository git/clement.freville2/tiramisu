use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct Recipe {
    pub id: usize,
    pub name: String,
    pub description: String,
    pub image: Option<String>,
    pub ingredients: Vec<IngredientEntry>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Ingredient {
    pub id: usize,
    pub name: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct IngredientEntry {
    pub id_ingredient: usize,
    pub quantity: usize,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct RecipeExtract {
    pub name: String,
    pub description: String,
    pub image: String,
}
