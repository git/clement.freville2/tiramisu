use crate::handler::{
    delete_recipe, edit_recipe, insert_recipe, list_ingredients, list_recipes, not_found,
};
use crate::recipe::{Ingredient, IngredientEntry, Recipe};
use axum::{
    http::{HeaderValue, Method},
    routing::{delete, get, post, put},
    Router,
};
use std::process::{ExitCode, Termination};
use std::sync::{Arc, RwLock};
use tower_http::cors::{Any, CorsLayer};

mod handler;
mod recipe;

#[derive(Clone)]
struct AppState {
    recipes: Arc<RwLock<Vec<Recipe>>>,
    ingredients: Arc<RwLock<Vec<Ingredient>>>,
}

impl Default for AppState {
    fn default() -> Self {
        AppState {
            recipes: Arc::new(RwLock::new(vec![Recipe {
                id: 1,
                name: "Spaghetti Carbonara".to_owned(),
                description: "A simple pasta dish".to_owned(),
                image: None,
                ingredients: vec![
                    IngredientEntry {
                        id_ingredient: 1,
                        quantity: 500,
                    },
                    IngredientEntry {
                        id_ingredient: 2,
                        quantity: 2,
                    },
                    IngredientEntry {
                        id_ingredient: 3,
                        quantity: 100,
                    },
                ],
            }])),
            ingredients: Arc::new(RwLock::new(vec![
                Ingredient {
                    id: 1,
                    name: "Pasta".to_owned(),
                },
                Ingredient {
                    id: 2,
                    name: "Egg".to_owned(),
                },
                Ingredient {
                    id: 3,
                    name: "Bacon".to_owned(),
                },
            ])),
        }
    }
}

#[tokio::main]
async fn main() -> impl Termination {
    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST, Method::PUT, Method::DELETE])
        .allow_headers(Any)
        .allow_origin(
            option_env!("ALLOW_ORIGIN")
                .unwrap_or("http://localhost:4200")
                .parse::<HeaderValue>()
                .unwrap(),
        );

    let app = Router::new()
        .route("/recipes", get(list_recipes))
        .route("/recipes", post(insert_recipe))
        .route("/recipes/:recipe_id", put(edit_recipe))
        .route("/recipes/:recipe_id", delete(delete_recipe))
        .route("/ingredients", get(list_ingredients))
        .fallback(not_found)
        .with_state(AppState::default())
        .layer(cors);

    match tokio::net::TcpListener::bind("0.0.0.0:3000").await {
        Ok(listener) => {
            println!("Listening on: http://{}", listener.local_addr().unwrap());
            axum::serve(listener, app).await.unwrap();
            ExitCode::SUCCESS
        }
        Err(err) => {
            eprintln!("Failed to bind: {err}");
            ExitCode::FAILURE
        }
    }
}
