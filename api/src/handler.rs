use crate::recipe::{Recipe, RecipeExtract};
use crate::AppState;
use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::Json;

pub(crate) async fn list_recipes(State(state): State<AppState>) -> impl IntoResponse {
    Json(state.recipes.read().unwrap().to_vec())
}

pub(crate) async fn insert_recipe(
    State(state): State<AppState>,
    Json(payload): Json<RecipeExtract>,
) -> StatusCode {
    let mut recipes = state.recipes.write().unwrap();
    let id = recipes.len();
    recipes.push(Recipe {
        id,
        name: payload.name,
        description: payload.description,
        image: None,
        ingredients: Vec::new(),
    });
    StatusCode::CREATED
}

pub(crate) async fn edit_recipe(
    State(state): State<AppState>,
    Path(recipe_id): Path<usize>,
    Json(payload): Json<RecipeExtract>,
) -> StatusCode {
    let mut recipes = state.recipes.write().unwrap();
    if let Some(recipe) = recipes.get_mut(recipe_id) {
        recipe.name = payload.name;
        recipe.description = payload.description;
        StatusCode::OK
    } else {
        StatusCode::NOT_FOUND
    }
}

pub(crate) async fn delete_recipe(
    State(state): State<AppState>,
    Path(recipe_id): Path<usize>,
) -> StatusCode {
    let mut recipes = state.recipes.write().unwrap();
    if let Some(recipe) = recipes.iter().position(|r| r.id == recipe_id) {
        recipes.remove(recipe);
        StatusCode::OK
    } else {
        StatusCode::NOT_FOUND
    }
}

pub(crate) async fn list_ingredients(State(state): State<AppState>) -> impl IntoResponse {
    Json(state.ingredients.read().unwrap().to_vec())
}

pub(crate) async fn not_found() -> impl IntoResponse {
    (StatusCode::NOT_FOUND, Json("nothing to see here"))
}
